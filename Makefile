all: run

clean:
	rm -f testabib dadosnovos.bin

build: clean
	gcc -o testabib testabib.c bibalunos.c

run: build
	./testabib

check: build
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./testabib
