/*
  AO PREENCHER ESTE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP,
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESTE ARQUIVO.
  TODAS AS PARTES ORIGINAIS DESTE EXERCÍCIO PROGRAMA (EP) FORAM
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES
  DESTE EP E, PORTANTO, NÃO CONSTITUEM DESONESTIDADE ACADÊMICA
  OU PLÁGIO.
  DECLARO TAMBÉM QUE SOU RESPONSÁVEL POR TODAS AS CÓPIAS DESTE
  ARQUIVO E QUE NÃO DISTRIBUÍ OU FACILITEI A SUA DISTRIBUIÇÃO.

  Nome : João Henri Carrenho Rocha
  NUSP : 11796378

  Referências: Com exceção das funções fornecidas no exercício
  e em aula, caso você tenha utilizado alguma referência,
  liste-a abaixo para que o seu programa não seja considerado
  plágio ou irregular.

  - Os métodos de ordenação de lista de struct foram baseados em
https://stackoverflow.com/questions/33979156/sorting-strings-and-structures-alphabetically-in-c
*/
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "bibalunos.h"

/*
 * Função: destroiListaAlunos
 * ---------------------------------
 * Esta função desaloca uma lista de alunos alocada dinamicamente.
 */
void destroiListaAlunos(tListaAlunos *lista)
{
    if (lista) {
        free(lista->alunos);
        free(lista);
    }
}

/*
 * Função: exibeAluno
 * ---------------------------------
 * Esta função imprime na saída padrão o registro de um aluno.
 */
void exibeAluno(tAluno aluno)
{
    printf("NUSP: %-7d Nome: %-30s Media: %4.2f\n",
           aluno.nusp, aluno.nome, aluno.media);
}

/*
 * Função: exibeListaAlunos
 * ---------------------------------
 * Esta função imprime na saída padrão os dados de uma lista de registros
 * de alunos. Cada linha impressa pela função exibe os dados de um aluno.
 */
void exibeListaAlunos(tListaAlunos lista)
{
    int i;
    for (i = 0; i < lista.tamanho; i++)
        exibeAluno(lista.alunos[i]);
}

/*
 * Função: carregaListaAlunos
 * ---------------------------------
 * Esta função lê os registros de alunos armazenados no arquivo binário
 * cujo nome é passado como parâmetro.
 * A função considera que o primeiro valor gravado no arquivo binário é um
 * número inteiro que indica a quantidade de registros de alunos que estão
 * gravados no mesmo arquivo na sequência. Um registro de aluno é um valor
 * do tipo tAluno.
 * Os dados lidos do arquivo são armazenados em uma estrutura do tipo
 * tListaAlunos, alocada pela função. A função devolve um ponteiro para
 * essa estrutura.
 */
tListaAlunos* carregaListaAlunos(char *nomeArq) {
    FILE* arq;
    tAluno aluno;
    tListaAlunos* alunos;
    int i;

    arq = fopen(nomeArq, "rb");
    if (!arq) {
        puts("Não foi possível abrir o arquivo para leitura.");
        exit(EXIT_FAILURE);
    }

    /*
     * A primeira sequência de caracteres refere-se à quantidade de alunos que
     * contêm no arquivo binário. Usaremos esta quantia para alocar o vetor
     * alunos de tListaAlunos.
     */
    alunos = malloc(sizeof(tListaAlunos));
    if (fread(&alunos->tamanho, sizeof(int), 1, arq) != 1) {
        printf("Falha na leitura do arquivo.");
        exit(EXIT_FAILURE);
    }

    alunos->alunos = malloc(sizeof(tAluno) * alunos->tamanho);

    i = 0;
    while (fread(&aluno, sizeof(tAluno), 1, arq) == 1) {
        alunos->alunos[i] = aluno;
        i++;
    }

    fclose(arq);
    return alunos;
}

void gravaListaAlunos(tListaAlunos lista, char *nomeArq) {
    FILE* arq;
    int i;

    arq = fopen(nomeArq, "wb");
    if (!arq) {
        printf("Não foi possível abrir o arquivo para leitura");
        exit(EXIT_FAILURE);
    }

    fwrite(&lista.tamanho, sizeof(int), 1, arq);
    for (i = 0; i < lista.tamanho; i++) {
        fwrite(&lista.alunos[i], sizeof(tAluno), 1, arq);
    }

    fclose(arq);
}

int compararNomes(const void* a, const void* b) {
    return strcmp(((tAluno *)a)->nome, ((tAluno*)b)->nome);
}

void ordenaListaAlunosPorNome(tListaAlunos lista) {
    int i;
    tAluno* alunos;

    alunos = malloc(sizeof(tAluno) * lista.tamanho);
    for (i = 0; i < lista.tamanho; i++) {
        alunos[i] = lista.alunos[i];
    }

    qsort(alunos, lista.tamanho, sizeof(tAluno), compararNomes);
    for (i = 0; i < lista.tamanho; i++) {
        lista.alunos[i] = alunos[i];
    }

    free(alunos);
}