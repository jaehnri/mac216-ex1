/*
*  ATENÇÃO: NÃO MODIFIQUE ESTA IMPLEMENTAÇÃO DA FUNÇÃO MAIN.
*  ELA É USADA NA AVALIAÇÃO AUTOMÁTICA DO EXERCÍCIO.
*/
#include <stdio.h>
#include "bibalunos.h"

/* Programa que testa as funções da biblioteca bibalunos */
int main()
{
    int modo_operacao;
    tListaAlunos *pLista;
    tListaAlunos listaTeste;
    tAluno alunos[5];

    alunos[0] = (tAluno){12345, "Penny", 2.1};
    alunos[1] = (tAluno){12789, "Sheldon Cooper", 10.0};
    alunos[2] = (tAluno){11523, "Leonard Hofstadter", 9.2};
    alunos[3] = (tAluno){13449, "Amy Farrah Fowler", 4.3};
    alunos[4] = (tAluno){11523, "Raj Koothrappali", 7.5};
    listaTeste.alunos = alunos;
    listaTeste.tamanho = 5;

    printf("Digite modo do programa: ");
    scanf("%d", &modo_operacao);

    switch (modo_operacao)
    {
        case 1: /* Testa a leitura de dados de alunos de um arquivo */
            pLista = carregaListaAlunos("dadosalunos.bin");
            puts("*** ALUNOS CARREGADOS DO ARQUIVO EXISTENTE ***");
            exibeListaAlunos(*pLista);
            destroiListaAlunos(pLista);
            break;

        case 2: /* Testa a gravação de dados de alunos em um arquivo */
            gravaListaAlunos(listaTeste, "dadosnovos.bin");
            pLista = carregaListaAlunos("dadosnovos.bin");
            puts("*** ALUNOS CARREGADOS DO NOVO ARQUIVO ***");
            exibeListaAlunos(*pLista);
            destroiListaAlunos(pLista);
            break;

        case 3: /* Testa a ordenação de lista de alunos */
            puts("*** ALUNOS ANTES DA ORDENACAO ***");
            exibeListaAlunos(listaTeste);
            ordenaListaAlunosPorNome(listaTeste);
            puts("*** ALUNOS DEPOIS DA ORDENACAO ***");
            exibeListaAlunos(listaTeste);
            break;

        case 4: /* Testa o uso combinado de todas as operações  */
            pLista = carregaListaAlunos("dadosalunos.bin");
            puts("*** ALUNOS CARREGADOS DO ARQUIVO EXISTENTE ***");
            exibeListaAlunos(*pLista);
            ordenaListaAlunosPorNome(*pLista);
            gravaListaAlunos(*pLista, "dadosordenados.bin");
            destroiListaAlunos(pLista);
            pLista = carregaListaAlunos("dadosordenados.bin");
            puts("*** ALUNOS CARREGADOS DO NOVO ARQUIVO ORDENADO ***");
            exibeListaAlunos(*pLista);
            destroiListaAlunos(pLista);
            break;

        default:
            puts("Modo de operação inválido!");
    }
    return 0;
}
