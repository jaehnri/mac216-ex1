/*
* ATENÇÃO: NÃO MODIFIQUE ESTA INTERFACE.
*/

#define TAM_MAX_NOME_ALUNO 31

/* Tipo de um registro de aluno */
typedef struct
{
    int nusp;
    char nome[TAM_MAX_NOME_ALUNO];
    float media;
} tAluno;

/* Tipo de uma lista de registros de alunos */
typedef struct
{
    int tamanho;    /* quantidade de registros de alunos na lista */
    tAluno *alunos; /* vetor de registros de alunos */
} tListaAlunos;

/*
 * Função: exibeAluno
 * ---------------------------------
 * Esta função imprime na saída padrão o registro de um aluno.
 */
void exibeAluno(tAluno aluno);

/*
 * Função: exibeListaAlunos
 * ---------------------------------
 * Esta função imprime na saída padrão os dados uma lista de registros de
 * alunos. Cada linha impressa pela função exibe os dados de um aluno.
 */
void exibeListaAlunos(tListaAlunos lista);

/*
 * Função: carregaListaAlunos
 * ---------------------------------
 * Esta função lê os registros de alunos armazenados no arquivo binário
 * cujo nome é passado como parâmetro.
 * A função considera que o primeiro valor gravado no arquivo binário é um
 * número inteiro que indica a quantidade de registros de alunos que estão
 * gravados no mesmo arquivo na sequência. Um registro de aluno é um valor
 * do tipo tAluno.
 * Os dados lidos do arquivo são armazenados em uma estrutura do tipo
 * tListaAlunos, alocada pela função. A função devolve um ponteiro para
 * essa estrutura.
 */
tListaAlunos *carregaListaAlunos(char *nomeArq);

/*
 * Função: destroiListaAlunos
 * ---------------------------------
 * Esta função desaloca uma lista de alunos alocada dinamicamente.
 */
void destroiListaAlunos(tListaAlunos *lista);

/*
 * Função: gravaListaAlunos
 * ---------------------------------
 * Esta função grava em um arquivo binário os dados da lista de alunos
 * passada como parâmetro. O nome do arquivo também é passado como
 * parâmetro para a função.
 * A função sobreescreve os dados do arquivo caso ele já exista.
 * O primeiro valor que a função grava no arquivo é um número inteiro
 * que indica a quantidade de registros de alunos que a lista contém.
 * Na sequência, a função grava os registros de alunos (valores do tipo
 * tAluno).
 */
void gravaListaAlunos(tListaAlunos lista, char *nomeArq);

/*
 * Função: ordenaListaAlunosPorNome
 * ---------------------------------
 * Esta função ordena uma lista de alunos pelo campo nome existente
 * nos registros de alunos.
 * A função rearranja os registros no vetor de alunos da lista,
 * deixando-os em ordem crescente de nome de aluno.
 */
void ordenaListaAlunosPorNome(tListaAlunos lista);
